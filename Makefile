###
### Configuration
###
OUTPUT_DIR=out
DELIVERY_DIR=delivery
PROJECT=Charpentier_H.9_Messe_Minuit

NENUVAR_LIB_PATH:=$(shell pwd)/../../nenuvar-lib
LILYPOND_OPTIONS=--loglevel=WARN -ddelete-intermediate-files -dno-protected-scheme-parsing
LILYPOND_CMD=lilypond -I$(shell pwd) -I$(NENUVAR_LIB_PATH) $(LILYPOND_OPTIONS)

###
### Scores and output files
###
PARTS_auto=dessus haute-contre haute-contre-sol taille basse basse-continue choeur
PARTS=$(PARTS_auto)
ALL_SCORES=$(PARTS) PO
OUTPUT_FILES=$(patsubst %,$(PROJECT)_%.pdf,$(ALL_SCORES)) \
             $(patsubst %,$(PROJECT)_transpo_%.pdf,$(ALL_SCORES))

###
### Specific and generic part rules
###
conducteur:
	$(LILYPOND_CMD) -o '$(OUTPUT_DIR)/$(PROJECT)_PO' main.ly
	$(LILYPOND_CMD) -o '$(OUTPUT_DIR)/$(PROJECT)_transpo_PO' -dtranspo main.ly
.PHONY: conducteur


define PART_template
 $(1):
	$(LILYPOND_CMD) -o '$(OUTPUT_DIR)/$(PROJECT)_$(1)' -dpart=$(1) part.ly
	$(LILYPOND_CMD) -o '$(OUTPUT_DIR)/$(PROJECT)_transpo_$(1)' -dpart=$(1) -dtranspo part.ly
 .PHONY: $(1)
endef
$(foreach part,$(PARTS_auto),$(eval $(call PART_template,$(part))))

parts: $(PARTS)
.PHONY: parts

###
### Delivery rules
###
define DELIVER_FILE_template
	if [ -e '$(OUTPUT_DIR)/$(1)' ] ; then rm -f '$(DELIVERY_DIR)/$(1)'; mv -fv '$(OUTPUT_DIR)/$(1)' $(DELIVERY_DIR)/; fi;
endef

delivery:
	@mkdir -p $(DELIVERY_DIR)/
	@$(foreach file,$(OUTPUT_FILES),$(call DELIVER_FILE_template,$(file)))
	@if [ -e "$(OUTPUT_DIR)/$(CONDUCTEUR)-1.midi" ]; then tar zcf $(DELIVERY_DIR)/$(PROJECT)-midi.tar.gz $(OUTPUT_DIR)/$(PROJECT)*.midi; fi
.PHONY:  delivery

###
###
###
all: check parts conducteur delivery clean

check:
	@if [ ! -d $(NENUVAR_LIB_PATH) ]; then \
	  echo "Please install nenuvar-lib in grand-parent directory:"; \
	  echo " cd ../.. && git clone https://github.com/nsceaux/nenuvar-lib.git"; \
	  false; \
	fi
clean:
	@rm -f $(OUTPUT_DIR)/$(PROJECT)_* $(OUTPUT_DIR)/$(PROJECT).*

.PHONY: clean all check
