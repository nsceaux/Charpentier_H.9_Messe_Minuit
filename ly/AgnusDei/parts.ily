\piecePartSpecs
#`((dessus #:score-template "score")
   (haute-contre)
   (haute-contre-sol)
   (taille)
   (basse)
   (choeur)
   (basse-continue)
   (silence #:on-the-fly-markup , #{ \markup\tacet#64 #}))
