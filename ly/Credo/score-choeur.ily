\score {
  <<
    \new Staff \with { \tinyStaff \haraKiri } <<
      \global \includeNotes "dessus"
    >>
    \new ChoirStaff \with { \haraKiri }<<
      \new Staff \withLyrics <<
        \global \includeNotes "voix"
        >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \includeNotes "voix-haute-contre"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \includeNotes "voix-taille"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \includeNotes "voix-basse"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
  >>
  \layout { indent = \smallindent }
}
