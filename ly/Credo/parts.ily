\piecePartSpecs
#`((dessus #:score-template "score")
   (haute-contre)
   (haute-contre-sol)
   (taille)
   (basse)
   (basse-continue)
   (choeur)
   (silence #:on-the-fly-markup , #{ \markup\tacet#128 #}))
