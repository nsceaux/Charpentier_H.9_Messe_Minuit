\pieceTocNb "Kyrie" \markup { Joseph est bien marié }
\includeScore "Kyrie"
\noPageBreak\markup\huge { Icy l’orgue joue le mesme Noël }
\noPageBreak\markup\vspace #2
\newBookPart#'(full-rehearsal)

\pieceTocNb "" \markup { Or nous dites Marie }
\includeScore "Christe"
\newBookPart#'(full-rehearsal)

\pieceTocNb "" \markup { Une jeune pucelle }
\includeScore "KyrieB"
\noPageBreak\markup\huge { Icy l’orgue joue le mesme Noël }
\noPageBreak\markup\vspace #2
\noPageBreak\markup\on-the-fly
#(lambda (layout props arg)
   (if (symbol? (*part*))
       empty-stencil
       (interpret-markup layout props arg)))
\huge {
  Tournez viste pour le \italic { gloria in excelsis deo }
  pendant que le celebrant l’entonne
}
\newBookPart#'(full-rehearsal)

\pieceTitleTocNb "Gloria in Excelsis Deo" ""
\markup\wordwrap {
  Les bourgeois de Chastre – Ou s’en vont ces guays bergers
}
\includeScore "Gloria"
\newBookPart#'(full-rehearsal)

\pieceTitleTocNb "Credo in unum Deum" ""
\markup { Vous qui désirez sans fin }
\includeScore "Credo"
\newBookPart#'(full-rehearsal)

\pieceTocNb "" \markup { Voicy le jour solemnel de noël }
\includeScore "Crucifixus"
\newBookPart#'(full-rehearsal)

\pieceTitleTocNb "" "" \markup { A la venue de Noel }
\includeScore "EtAscendit"
\newBookPart#'(full-rehearsal)

\markup\on-the-fly
#(lambda (layout props arg)
   (if (symbol? (*part*))
       (interpret-markup layout props " ")
       (interpret-markup layout props arg)))
\huge\wordwrap {
  A l’offertoire les vi[ol]ons joueront
  \italic { Laissez paitre vos bestes }
  en d la re sol \raise #1 \musicglyph #"accidentals.sharp"
}

\pieceTocNb "Offertoire" "Laissez paitre vos beste"
\includeScore "LaissezPaitre"
\noPageBreak\markup\huge { Passez au Sanctus }
\noPageBreak\markup\vspace #2
\newBookPart#'(full-rehearsal)

\newBookPart #'(dessus)
\pieceTocNb "Sanctus" \markup { O dieu que n’etois je en vie }
\includeScore "Sanctus"
\newBookPart#'(full-rehearsal)

\pieceTocNb "Agnus Dei" \markup { A minuit fut fait un resveil }
\includeScore "AgnusDei"
