\piecePartSpecs
#`((dessus)
   (dessus-haute-contre)
   (haute-contre #:notes "dessus-haute-contre")
   (haute-contre-sol #:notes "dessus-haute-contre")
   (taille)
   (basse)
   (basse-continue #:score-template "score")
   (silence #:on-the-fly-markup , #{ \markup\tacet#50 #}))
