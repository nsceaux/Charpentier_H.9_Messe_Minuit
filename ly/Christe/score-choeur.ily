\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      <>^\markup { \concat { P \super re } seule }
      \global \keepWithTag #'vhaute-contre \includeNotes "voix"
    >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
    \new Staff \withLyrics <<
      <>^\markup { \concat { P \super re } seule }
      \global \keepWithTag #'vtaille \includeNotes "voix"
    >> \keepWithTag #'vtaille \includeLyrics "paroles"
    \new Staff \withLyrics <<
      <>^\markup { \concat { P \super re } seule }
      \global \keepWithTag #'vbasse \includeNotes "voix"
    >> \keepWithTag #'vbasse \includeLyrics "paroles"
  >>
  \layout { indent = \smallindent }
}