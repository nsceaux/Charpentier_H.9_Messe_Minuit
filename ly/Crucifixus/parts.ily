\piecePartSpecs
#`((dessus)
   (choeur)
   (basse-continue
    #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\tacet#30 #}))
