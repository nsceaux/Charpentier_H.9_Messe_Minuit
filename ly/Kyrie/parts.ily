\piecePartSpecs
#`((dessus #:score-template "score")
   (haute-contre)
   (haute-contre-sol)
   (taille)
   (choeur)
   (basse)
   (basse-continue)
   (silence #:on-the-fly-markup , #{ \markup\tacet#51 #}))
