\score {
  <<
    \new Staff \with { \haraKiri } <<
      \global \includeNotes "dessus"
      { s1*28\break \startHaraKiri }
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \includeNotes "voix-haute-contre"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \includeNotes "voix-taille"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \includeNotes "voix-basse"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
  >>
  \layout { indent = \smallindent }
}
