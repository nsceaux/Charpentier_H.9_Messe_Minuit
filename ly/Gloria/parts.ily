\piecePartSpecs
#`((dessus)
   (haute-contre #:notes "dessus-haute-contre"
                 #:tag-notes haute-contre
                 #:music , #{ s1*15 s1*68 s2.*32 \break #})
   (haute-contre-sol #:notes "dessus-haute-contre"
                 #:tag-notes haute-contre
                 #:music , #{ s1*15 s1*68 s2.*32 \break #})
   (taille #:music , #{ s1*15 s1*68 s2.*32 \break #})
   (basse #:music , #{ s1*15 s1*68 s2.*32 \break #})
   (basse-continue
    #:music , #{ s1*47\break\allowPageTurn s1*36 s2.*32\break #})
   (choeur)
   (silence #:on-the-fly-markup , #{ \markup\tacet#164 #}))
