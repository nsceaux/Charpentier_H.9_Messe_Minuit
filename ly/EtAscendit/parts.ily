\piecePartSpecs
#`((dessus)
   (haute-contre #:notes "dessus-haute-contre"
                 #:tag haute-contre)
   (haute-contre-sol #:notes "dessus-haute-contre"
                 #:tag haute-contre)
   (taille)
   (basse)
   (choeur)
   (basse-continue)
   (silence #:on-the-fly-markup , #{ \markup\tacet#138 #}))
