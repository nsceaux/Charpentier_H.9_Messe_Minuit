#(ly:set-option 'urtext #t)
\include "common.ily"

%% In urtext version, add original manuscript page numbers in page header
\paper {
  #(define original-page-number-table
     (let ((page 3) ;; first score page number
           (orig-pages '("Vol. 25 f. 62-r"
                         "Vol. 25 f. 62-v"
                         "Vol. 25 f. 63-r"
                         "Vol. 25 f. 63-v"
                         "Vol. 25 f. 64-r"
                         "Vol. 25 f. 64-v"
                         "Vol. 25 f. 65-r"
                         "Vol. 25 f. 65-v"
                         "Vol. 25 f. 66-r"
                         "Vol. 25 f. 66-v"
                         "Vol. 25 f. 67-r"
                         "Vol. 25 f. 67-v"
                         "Vol. 25 f. 68-r"
                         "Vol. 25 f. 68-v"
                         "Vol. 25 f. 69-r"
                         "Vol. 25 f. 69-v"
                         "Vol. 25 f. 70-r"
                         "Vol. 25 f. 70-v"
                         "Vol. 25 f. 71-r"
                         "Vol. 25 f. 71-v"
                         "Vol. 25 f. 72-r"
                         "Vol. 25 f. 72-v"
                         "Vol. 25 f. 73-r"
                         "Vol. 25 f. 73-v"
                         "Vol. 25 f. 74-r"
                         "Vol. 25 f. 74-v"
                         "Vol. 25 f. 75-r"
                         "Vol. 25 f. 75-v"
                         "Vol. 25 f. 76-r"
                         "Vol. 25 f. 76-v"
                         "Vol. 25 f. 77-r")))
       (map (lambda (orig-page)
              (set! page (1+ page))
              (cons (1- page) orig-page))
            orig-pages)))
}

#(define-markup-command (page-header layout props text) (markup?)
   (define (ancestor layout)
     "Return the topmost layout ancestor"
     (let ((parent (ly:output-def-parent layout)))
       (if (not (ly:output-def? parent))
           layout
           (ancestor parent))))
   (let* ((first-page-number (ly:output-def-lookup (ancestor layout) 'first-page-number))
          (page-number (chain-assoc-get 'page:page-number props -1))
          (page-number-markup (number->string page-number))
          (text-markup (markup #:italic (or text "")))
          (part-page-number (1+ (- page-number first-page-number)))
          (orig-page-number-table (ly:output-def-lookup layout 'original-page-number-table '()))
          (orig-page-number-markup (if (eqv? #t (ly:get-option 'urtext))
                                       (let ((num-text (assoc part-page-number orig-page-number-table)))
                                         (if (pair? num-text)
                                             (cdr num-text)
                                             ""))
                                       "")))
         (if (or (= page-number 1) (not text))
             empty-stencil
         (interpret-markup
          layout props
          (if (odd? page-number)
              (markup #:fill-line (orig-page-number-markup text-markup page-number-markup))
              (markup #:fill-line (page-number-markup text-markup orig-page-number-markup)))))))

%% Title page
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header {
    title = \markup\fontsize #-2 \center-column {
      \smallCaps "Messe de Minuit"
      \fontsize #-4 \wordwrap-center {
        A 4 voix, flûtes et violons pour Noël
      }
    }
    date = \markup\center-column { H.9 "ca. 1694" }
    editions = \markup \center-column {
      \line { Édition \italic urtext }
      \fontsize #-3 \line { Mélanges autographes, volume 25, feuillets 62 à 77 }
    }
  }
  \markup\null
}
%% Table of contents
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \markuplist
  \abs-fontsize-lines #8
  \with-line-width-ratio #0.7
  \override-lines #'(use-rehearsal-numbers . #f)
  \override-lines #'(column-number . 1)
  \table-of-contents
}

pieceTitleToc =
#(define-music-function (parser location title toc-title) (markup? markup?)
  (let ((rehearsal (rehearsal-number))
        (font-size (if (symbol? (ly:get-option 'part)) 2 3)))
    (add-toc-item parser 'tocPieceMarkup toc-title rehearsal)
    (add-toplevel-markup parser 
                         (markup #:fill-line (#:fontsize font-size title)))
    (add-no-page-break parser)
    (make-music 'Music 'void #t)))

\markup\fill-line {
  \center-column {
    \fontsize #5 \line { Messe de Minuit }
    \null
    \fontsize #4 \line { a 4 voix fl[utes] et vi[ol]ons pour Noël }
    \null
  }
}

\pieceTitleToc \markup\larger { Joseph est bien marié }
\markup { Kyrie — Joseph est bien marié }
\includeScore "Kyrie"

\pieceTitleToc \markup\larger { Or nous dites Marie }
\markup { Or nous dites Marie }
\includeScore "Christe"

\pieceTitleToc \markup\larger { Une jeune pucelle }
\markup { Une jeune pucelle }
\includeScore "KyrieB"

\pieceTitleToc
\markup\larger { Gloria in Excelsis Deo }
\markup\wordwrap {
  Gloria in Excelsis Deo — Les bourgeois de Chastre —
  Ou s’en vont ces guays bergers
}
\includeScore "Gloria"

\pieceTitleToc
\markup\larger { Credo in unum Deum }
\markup { Credo in unum Deum — Vous qui désirez sans fin }
\includeScore "Credo"

\pieceTitleToc "" \markup { Voicy le jour solemnel de noël }
\includeScore "Crucifixus"

\pieceTitleToc "" \markup { A la venue de Noel }
\includeScore "EtAscendit"

\pieceTitleToc
\markup\larger { O dieu que n’etois je en vie }
\markup { Sanctus — O dieu que n’etois je en vie }
\includeScore "Sanctus"
\noPageBreak\markup\fill-line {
  \null
  \column { \line { tournez pour } \line { l’Agnus dei } }
}
\pageBreak

\pieceTitleToc
\markup\larger { A minuit fut fait un resveil }
\markup { Agnus Dei — A minuit fut fait un resveil }
\includeScore "AgnusDei"
