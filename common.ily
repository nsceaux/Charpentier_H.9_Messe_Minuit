\version "2.19.54"

\header {
  copyrightYear = "2013"
  composer = "Marc-Antoine Charpentier"
  opus = "H.9"
  subtitle = \markup { À 4 voix, flûtes et violons pour Noël }
  date = "ca. 1694"
  editions = \markup\center-column {
    \fontsize #-3 \line { Mélanges autographes, volume 25, feuillets 62 à 77 }
  }
  arrangement = \markup\italic\fontsize#-3
  #(if (eqv? #t (ly:get-option 'transpo))
       "(transposition un ton en-dessous)"
       (make-null-markup))
}

#(ly:set-option 'original-layout (eqv? #t (ly:get-option 'urtext)))
#(ly:set-option 'use-ancient-clef (eqv? (ly:get-option 'urtext) #t))
#(ly:set-option 'show-ancient-clef (not (symbol? (ly:get-option 'part))))
#(ly:set-option 'baroque-repeats (eqv? #t (ly:get-option 'urtext)))
#(ly:set-option 'baroque-repeat-bar ":||:")
#(ly:set-option 'use-rehearsal-numbers (not (eqv? #t (ly:get-option 'urtext))))

%% Staff size:
%%  12 for urtext lead sheet
%%  14 for lead sheets
%%  16 for vocal parts
%%  18 for instruments
#(set-global-staff-size
  (cond ((memq (ly:get-option 'part) '(choeur)) 16) ;; vocal part
        ((ly:get-option 'part) 18)      ;; other parts
        ((eqv? #t (ly:get-option 'urtext)) 12) ;; make urtext smaller
        (else 16)))

%% Line/page breaking algorithm
%%  optimal   for lead sheets
%%  page-turn for instruments and vocal parts
\paper {
  #(define page-breaking
     (if (or (eqv? (ly:get-option 'part) #f)
             (eqv? (ly:get-option 'part) 'choeur))
         ly:optimal-breaking
         ly:page-turn-breaking))
}

\language "italiano"
\include "nenuvar-lib.ily"
\include "nenuvar-garamond.ily"
\setPath "ly"

\opusPartSpecs
#`((dessus "Dessus" ()
           (#:score "score-dessus" #:notes "dessus"
                    #:clef ,(if (eqv? (ly:get-option 'dessus-sol1) #t)
                                "french"
                                "treble")))
   (haute-contre "Hautes-contre" ()
                 (#:notes "haute-contre"
                  #:tag-notes haute-contre
                  #:clef "alto"))
   (haute-contre-sol "Hautes-contre" ((haute-contre #f))
                     (#:notes "haute-contre"
                              #:tag-notes haute-contre
                              #:clef "treble"))
   (taille "Tailles" () (#:notes "taille" #:clef "alto"))
   (basse "Basses" ()
          (#:notes "basse" #:clef "basse" #:tag-notes basse))
   (basse-continue "Basse continue" ()
          (#:notes "basse" #:clef "basse" #:tag-notes basse-continue
                   #:score-template "score-basse-voix"))
   (choeur "Chœur" () (#:score "score-choeur")))

%% Clés des dessus dans le conducteur
#(if (eqv? (ly:get-option 'dessus-sol1) #t)
     (assoc-set! clef-map 'dessus '(french . french)))

#(if (eqv? (ly:get-option 'part) 'haute-contre-sol)
     (assoc-set! clef-map 'haute-contre '(pettrucci-c1 . treble)))

\layout {
  indent = #(if (eqv? #t (ly:get-option 'urtext))
                  smallindent
                  largeindent)
  ragged-last = #(eqv? #t (ly:get-option 'urtext))
}

%% ton original ou tranposition un ton en dessous avec l'option `transpo'
global = 
#(define-music-function (parser this-location) ()
   (with-location #f
  (let* ((global-symbol
          (string->symbol (format #f "global~a~a" (*path*) (*piece*))))
         (global-music (ly:parser-lookup global-symbol)))
   (if (not (ly:music? global-music))
       (let* ((global-file (include-pathname "global")))
         (set! global-music
               (if (eqv? #t (ly:get-option 'transpo))
                   #{ \notemode { \transpose re do { \include $global-file } } #}
                   #{ \notemode { \include $global-file } #}))
         (ly:parser-define! global-symbol global-music)))
   (ly:music-deep-copy global-music))))

includeNotes = 
#(define-music-function (parser this-location pathname) (string?)
   (with-location #f
     (let ((include-file (include-pathname pathname)))
       (if (eqv? #t (ly:get-option 'transpo))
           #{ \notemode { \transpose re do { \include $include-file } } #}
           #{ \notemode { \include $include-file } #}))))
