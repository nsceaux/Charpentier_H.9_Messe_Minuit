\version "2.23.10"
\include "common.ily"

%% Title page
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header { title = "Messe de Minuit" }
  \markup\null
}
%% Table of contents
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \markuplist
  \abs-fontsize-lines #8
  \with-line-width-ratio #0.7
  \override-lines #'(use-rehearsal-numbers . #t)
  \override-lines #'(column-number . 1)
  \override-lines #'(rehearsal-number-gauge . "Gloria in Excelsis Deo ")
  \override-lines #`(rehearsal-number-align . ,LEFT)
  \table-of-contents
}
\include "ly/body.ily"
